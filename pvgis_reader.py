# time,P,G(i),H_sun,T2m,WS10m,Int
# time: YYYYMMDD:HHMM (weird format, I know)
# P: PV system power (W)
# G(i): Global irradiance on the inclined plane (plane of the array) (W/m2)
# H_sun: Sun height (degree)
# T2m: 2-m air temperature (degree Celsius)
# WS10m: 10-m total wind speed (m/s)
# Int: 1 means solar radiation values are reconstructed
# 20200101:0011,0.0,0.0,0.0,2.65,4.97,0.0

from collections.abc import Iterable
from typing import Callable

def download_pvgis_as_iterator(lat="56.000",
                               lon="12.000",
                               raddatabase="PVGIS-SARAH2",
                               browser=0,
                               outputformat="csv",
                               userhorizon="",
                               usehorizon=1,
                               angle=22,
                               aspect=0,
                               startyear=2020,
                               endyear=2020,
                               mountingplace="free",
                               optimalinclination=0,
                               optimalangles=0,
                               js=1,
                               select_database_hourly="PVGIS-SARAH2",
                               hstartyear=2020,
                               hendyear=2020,
                               trackingtype=0,
                               hourlyangle=22,
                               hourlyaspect=0,
                               pvcalculation=1,
                               pvtechchoice="crystSi",
                               peakpower=1,
                               loss=14) -> Iterable[str]:
    import urllib.request
    baseurl = "https://re.jrc.ec.europa.eu/api/v5_2/seriescalc?"
    url = f"{baseurl}lat={lat}&lon={lon}&raddatabase={raddatabase}&browser={browser}&outputformat={outputformat}&userhorizon={userhorizon}&usehorizon={usehorizon}&angle={angle}&aspect={aspect}&startyear={startyear}&endyear={endyear}&mountingplace={mountingplace}&optimalinclination={optimalinclination}&optimalangles={optimalangles}&js={js}&select_database_hourly={select_database_hourly}&hstartyear={hstartyear}&hendyear={hendyear}&trackingtype={trackingtype}&hourlyangle={hourlyangle}&hourlyaspect={hourlyaspect}&pvcalculation={pvcalculation}&pvtechchoice={pvtechchoice}&peakpower={peakpower}&loss={loss}"

    lines = urllib.request\
        .urlopen(url)\
        .read()\
        .decode("utf-8")\
        .splitlines()
    
    return iter(lines)

def read_pvgis(iterator: Iterable[str],
               setup: Callable[[dict], None]) -> dict:
    res = {}

    res["pvgis_latitude"] =\
        next(iterator).strip()\
        .split(":")[1]\
        .strip()

    res["pvgis_longitude"] =\
        next(iterator).strip()\
        .split(":")[1]\
        .strip()
    
    res["pvgis_elevation"] =\
        next(iterator).strip()\
        .split(":")[1]\
        .strip()

    res["pvgis_database"] =\
        next(iterator).strip()\
        .split(":")[1]\
        .strip()

    # skip empty lines
    next(iterator)
    next(iterator)

    res["pvgis_slope"] =\
        next(iterator).strip()\
        .split(":")[1]\
        .strip()\
        .split(" ")[0]\
        .strip()
    
    res["pvgis_azimuth"] =\
        next(iterator).strip()\
        .split(":")[1]\
        .strip()\
        .split(" ")[0]\
        .strip()
    
    res["pvgis_power"] =\
        next(iterator).strip()\
        .split(":")[1]\
        .strip()
    
    res["pvgis_losses"] =\
        next(iterator).strip()\
        .split(":")[1]\
        .strip()
    
    # skip header
    next(iterator)

    calculator = setup(res)

    for row_raw in iterator:
        row = row_raw.strip()
        if not row:
            break
        calculator(res, row)

    return res